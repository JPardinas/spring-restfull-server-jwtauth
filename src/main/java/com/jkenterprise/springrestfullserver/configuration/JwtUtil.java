package com.jkenterprise.springrestfullserver.configuration;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUtil {
	
	public static void addAuthentication (HttpServletResponse res, String username) {
		
		String token = Jwts.builder()
				.setSubject(username)
				.signWith(SignatureAlgorithm.HS512, "Cl@v3")
				.compact();
		
		res.addHeader("Authorization", "Bearer " + token);
		
	}

	public static Authentication getAuthentication(HttpServletRequest request) {
		
		String token = request.getHeader("Authorization");
		
		if (token != null) {
			String user = Jwts.parser()
					.setSigningKey("Cl@v3")
					.parseClaimsJws(token.replace("Bearer", ""))
					.getBody()
					.getSubject();
			
			return user != null ? new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList()) : null;
		}
		
		
		
		return null;
	}

}
