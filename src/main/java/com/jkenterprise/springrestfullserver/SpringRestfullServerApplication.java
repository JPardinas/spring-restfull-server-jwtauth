package com.jkenterprise.springrestfullserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestfullServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestfullServerApplication.class, args);
	}

}
