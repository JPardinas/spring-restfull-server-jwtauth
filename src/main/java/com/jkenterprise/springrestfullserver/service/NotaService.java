package com.jkenterprise.springrestfullserver.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jkenterprise.springrestfullserver.converter.Convertidor;
import com.jkenterprise.springrestfullserver.entity.Nota;
import com.jkenterprise.springrestfullserver.model.MNota;
import com.jkenterprise.springrestfullserver.repository.NotaRepositorio;

@Service("servicio")
public class NotaService {
	@Autowired
	@Qualifier("repositorio")
	private NotaRepositorio repositorio;
	
	@Autowired
	@Qualifier("convertidor")
	private Convertidor convertidor;
	
	private static final Log logger = LogFactory.getLog(NotaService.class);
	
	public boolean crear (Nota nota) {
		logger.info("CREANDO NOTA");
		try {
			repositorio.save(nota);
			logger.info("NOTA CREADA");
			return true;
		} catch (Exception e) {
			logger.error("HUBO UN ERROR CREANDO LA NOTA");
			return false;
		}
	}
	
	public boolean actualizar (Nota nota) {
		logger.info("ACTUALIZANDO NOTA");
		try {
			logger.info("NOTA ACTUALIZADA");
			repositorio.save(nota);
			return true;
		} catch (Exception e) {
			logger.error("HUBO UN ERROR ACTUALIZANDO LA NOTA");
			return false;
		}
	}
	
	public boolean borrar (String nombre, long id) {
		logger.warn("BORRANDO NOTA");
		try {
			logger.info("NOTA BORRADA");
			Nota nota = repositorio.findByNombreAndId(nombre, id);
			repositorio.delete(nota);
			return true;
		} catch (Exception e) {
			logger.error("HUBO UN ERROR BORRANDO LA NOTA");
			return false;
		}
	}
	
	public List<MNota> obtener() {
		logger.error("OBTENIENDO TODOS LOS ELEMENTOS");
		return convertidor.convertirLista(repositorio.findAll());
	}
	
	public MNota obtenerPorNombreTitulo (String nombre, String titulo) {
		return new MNota(repositorio.findByNombreAndTitulo(nombre, titulo));
	}
	
	public List<MNota> obtenerTitulo (String titulo) {
		return convertidor.convertirLista(repositorio.findByTitulo(titulo));
	}
	
	public List<MNota> obtenerPorPaginacion (Pageable pageable) {
		return convertidor.convertirLista(repositorio.findAll(pageable).getContent());
	}
	
}
